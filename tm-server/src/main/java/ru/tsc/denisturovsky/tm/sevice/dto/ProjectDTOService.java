package ru.tsc.denisturovsky.tm.sevice.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.enumerated.CustomSort;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.denisturovsky.tm.exception.field.*;
import ru.tsc.denisturovsky.tm.repository.dto.ProjectDTORepository;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectDTOService extends AbstractUserOwnedDTOService<ProjectDTO> implements IProjectDTOService {

    @Nullable
    @Autowired
    private ProjectDTORepository repository;

    @Override
    @Transactional
    public void changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        repository.save(project);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        return repository.save(project);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return repository.save(project);
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (dateBegin == null || dateEnd == null) throw new IncorrectDateException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return repository.save(project);
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.existByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findByUserId(userId);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(
            @Nullable final String userId,
            @Nullable final CustomSort sort
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @Nullable final Sort findSort;
        switch (sort.name()) {
            case "BY_NAME":
                findSort = Sort.by(Sort.Direction.ASC, "name");
                break;
            case "BY_STATUS":
                findSort = Sort.by(Sort.Direction.ASC, "status");
                break;
            default:
                findSort = Sort.by(Sort.Direction.ASC, "created");
        }
        return repository.findByUserId(userId, findSort);
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<ProjectDTO> result = repository.findByUserIdAndId(userId, id);
        return result.orElse(null);
    }

    @Nullable
    protected ProjectDTORepository getRepository() {
        return repository;
    }

    @Override
    public int getSize(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return (int) repository.countByUserId(userId);
    }

    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (findOneById(userId, id) == null) throw new EntityNotFoundException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void updateOneById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
    }

}
