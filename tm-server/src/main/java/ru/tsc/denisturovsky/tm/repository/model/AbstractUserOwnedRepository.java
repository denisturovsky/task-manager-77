package ru.tsc.denisturovsky.tm.repository.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.denisturovsky.tm.model.AbstractUserOwnedModel;

@Repository
@Scope("prototype")
public interface AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> {

}