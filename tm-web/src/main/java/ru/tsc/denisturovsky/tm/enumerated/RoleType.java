package ru.tsc.denisturovsky.tm.enumerated;

public enum RoleType {

    ADMINISTRATOR,
    USER;

}