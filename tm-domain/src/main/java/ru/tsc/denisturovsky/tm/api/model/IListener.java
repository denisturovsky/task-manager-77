package ru.tsc.denisturovsky.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IListener {

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

    @NotNull
    String getName();

}
